// ==UserScript==
// @id             iitc-plugin-lira_range_Simulator
// @name           IITC plugin: Portal Range Simulator
// @category       Layer
// @author         +5aleel
// @version        0.0.1.20161010.0040
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      http://tg.lebcoders.net/js/LIRA_portal_range_simulator.js
// @downloadURL    http://tg.lebcoders.net/js/LIRA_portal_range_simulator.js
// @description    Simulate Portal Range On Map
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {


    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function () {};

    //PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
    //(leaving them in place might break the 'About IITC' page or break update checks)
    plugin_info.buildName = 'HakunaMatata';
    plugin_info.dateTimeVersion = '2016.10.10.00.40';
    plugin_info.pluginId = 'lira_range_Simulator';
    //END PLUGIN AUTHORS NOTE

    // PLUGIN START ////////////////////////////////////////////////////////

    /* use own namespace for plugin */
    window.plugin.lira_range_Simulator = function(){};
    window.plugin.lira_range_Simulator.circleLayers = {};
    window.plugin.lira_range_Simulator.previousGUID=0;
    window.plugin.lira_range_Simulatororignal_portal_data=null;
    window.plugin.lira_range_Simulator.portalAdded = function(data) {
    data.portal.on('add', function() {
      window.plugin.lira_range_Simulator.draw(this.options.guid);
    });
    data.portal.on('remove', function() {
      window.plugin.lira_range_Simulator.remove(this.options.guid);
    });
  };

  window.plugin.lira_range_Simulator.remove = function(guid) {
      if(guid!==0){
    var previousLayer = window.plugin.lira_range_Simulator.circleLayers[guid];
    if(previousLayer) {
        window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup.removeLayer(previousLayer);
      delete window.plugin.lira_range_Simulator.circleLayers[guid];
    }
      }
  };

  window.plugin.lira_range_Simulator.removePrev = function() {
    window.plugin.lira_range_Simulator.remove(window.plugin.lira_range_Simulator.previousGUID);
  };
  window.plugin.lira_range_Simulator.draw = function(guid,range) {
    var d = window.portals[guid];
      var coo = d._latlng;
      var latlng = new L.LatLng(coo.lat,coo.lng);
      var portalLevel = d.options.level;
      var optCircle = {color:'DarkBlue',opacity:1,fillColor:'LightGrey',fillOpacity:0.2,weight:1,clickable:false, dashArray: [10,6]};
      var circle = new L.geodesicCircle(latlng, range, optCircle);
        circle.addTo(window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup);
      window.plugin.lira_range_Simulator.circleLayers[guid] = circle;

  };
window.plugin.lira_range_Simulator.showOrHide = function() {

      // show the layer
      if(!window.plugin.lira_range_Simulator.rangeSimLayerHolderGroup.hasLayer(window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup)) {
        window.plugin.lira_range_Simulator.rangeSimLayerHolderGroup.addLayer(window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup);
        $('.leaflet-control-layers-list span:contains("Range Simulator")').parent('label').removeClass('disabled').attr('title', '');
      }
  };

    var setup = function () {


    window.plugin.lira_range_Simulator.rangeSimLayerHolderGroup = new L.LayerGroup();
    $("body").append("<style>#randdetails tbody tr:nth-child(1) th:nth-child(3){cursor:pointer;color:red}</style>");
    // this layer is added into the above layer, and removed from it when we zoom out too far
    window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup = new L.LayerGroup();
    window.plugin.lira_range_Simulator.rangeSimLayerHolderGroup.addLayer(window.plugin.lira_range_Simulator.rangeSimCircleHolderGroup);
    window.addLayerGroup('Portal Range Simulator', window.plugin.lira_range_Simulator.rangeSimLayerHolderGroup, true);
    window.plugin.lira_range_Simulator.showOrHide();
        function simpleModName(t)
        {
            if(t!=='undefined' && t!== null && t!=='')
                return t.rarity + ' ' + t.name;
            else
                return "none";
        }
  $(document).on('click','#randdetails tbody tr:first th:nth-child(3):not(.loaded)',function(){
      $(this).addClass("loaded");
      var mods=window.plugin.lira_range_Simulator.orignal_portal_data.mods;
      mods[0]=simpleModName(mods[0]);
      mods[1]=simpleModName(mods[1]);
      mods[2]=simpleModName(mods[2]);
      mods[3]=simpleModName(mods[3]);
      //alert(JSON.stringify(mods));
            $('#portaldetails > .mods > span:nth-child(1)').prepend('<select class="lvl_sim_change_trg"><option '+((mods[0]=='none')?'selected="selected"':'')+' value="none">none</option><option '+((mods[0]=="RARE Link Amp")?'selected="selected"':'')+' value="RLA">RLA</option><option '+((mods[0]=="VERY_RARE Link Amp")?'selected="selected"':'')+' value="VRLA">VRLA</option><option '+((mods[0]=="VERY_RARE SoftBank Ultra Link")?'selected="selected"':'')+' value="SBUL">SBUL</option></select>');
            $('#portaldetails > .mods > span:nth-child(2)').prepend('<select class="lvl_sim_change_trg"><option '+((mods[1]=='none')?'selected="selected"':'')+' value="none">none</option><option '+((mods[1]=="RARE Link Amp")?'selected="selected"':'')+' value="RLA">RLA</option><option '+((mods[1]=="VERY_RARE Link Amp")?'selected="selected"':'')+' value="VRLA">VRLA</option><option '+((mods[1]=="VERY_RARE SoftBank Ultra Link")?'selected="selected"':'')+' value="SBUL">SBUL</option></select>');
            $('#portaldetails > .mods > span:nth-child(3)').prepend('<select class="lvl_sim_change_trg"><option '+((mods[2]=='none')?'selected="selected"':'')+' value="none">none</option><option '+((mods[2]=="RARE Link Amp")?'selected="selected"':'')+' value="RLA">RLA</option><option '+((mods[2]=="VERY_RARE Link Amp")?'selected="selected"':'')+' value="VRLA">VRLA</option><option '+((mods[2]=="VERY_RARE SoftBank Ultra Link")?'selected="selected"':'')+' value="SBUL">SBUL</option></select>');
            $('#portaldetails > .mods > span:nth-child(4)').prepend('<select class="lvl_sim_change_trg"><option '+((mods[3]=='none')?'selected="selected"':'')+' value="none">none</option><option '+((mods[3]=="RARE Link Amp")?'selected="selected"':'')+' value="RLA">RLA</option><option '+((mods[3]=="VERY_RARE Link Amp")?'selected="selected"':'')+' value="VRLA">VRLA</option><option '+((mods[3]=="VERY_RARE SoftBank Ultra Link")?'selected="selected"':'')+' value="SBUL">SBUL</option></select>');
            $('#resodetails > tbody > tr > th').each(function(){
                var lvl_shloud_not_confilct;
                if($(this).children("span.meter").has("span.meter-level").length){
                    lvl_shloud_not_confilct = $.trim($(this).children("span.meter").children("span.meter-level").text()).substring(0, 3);
                $(this).children("span.meter").children("span.meter-level").html('<select class="lvl_sim_change_trg reso"><option value="0" '+((lvl_shloud_not_confilct==="")?'selected="selected"':'')+' >0</option><option value="1" '+((lvl_shloud_not_confilct=="L 1")?'selected="selected"':'')+' >1</option><option value="2" '+((lvl_shloud_not_confilct=="L 2")?'selected="selected"':'')+' >2</option><option value="3" '+((lvl_shloud_not_confilct=="L 3")?'selected="selected"':'')+' >3</option><option value="4" '+((lvl_shloud_not_confilct=="L 4")?'selected="selected"':'')+' >4</option><option value="5" '+((lvl_shloud_not_confilct=="L 5")?'selected="selected"':'')+' >5</option><option value="6" '+((lvl_shloud_not_confilct=="L 6")?'selected="selected"':'')+' >6</option><option value="7" '+((lvl_shloud_not_confilct=="L 7")?'selected="selected"':'')+' >7</option><option value="8" '+((lvl_shloud_not_confilct=="L 8")?'selected="selected"':'')+' >8</option></select>');
                }else{
                    lvl_shloud_not_confilct = 0;
                $(this).children("span.meter").html('<select class="lvl_sim_change_trg reso"><option value="0" '+((lvl_shloud_not_confilct==0)?'selected="selected"':'')+' >0</option><option value="1" '+((lvl_shloud_not_confilct=="L 1")?'selected="selected"':'')+' >1</option><option value="2" '+((lvl_shloud_not_confilct=="L 2")?'selected="selected"':'')+' >2</option><option value="3" '+((lvl_shloud_not_confilct=="L 3")?'selected="selected"':'')+' >3</option><option value="4" '+((lvl_shloud_not_confilct=="L 4")?'selected="selected"':'')+' >4</option><option value="5" '+((lvl_shloud_not_confilct=="L 5")?'selected="selected"':'')+' >5</option><option value="6" '+((lvl_shloud_not_confilct=="L 6")?'selected="selected"':'')+' >6</option><option value="7" '+((lvl_shloud_not_confilct=="L 7")?'selected="selected"':'')+' >7</option><option value="8" '+((lvl_shloud_not_confilct=="L 8")?'selected="selected"':'')+' >8</option></select>');
                }
                //alert(lvl_shloud_not_confilct);
            });
  });
  $(document).on('change','.lvl_sim_change_trg',function(){
                    var reso_count=0;
                    var reso_total=0;
                    var mod_vrla=0;
                    var mod_sbul=0;
                    var mod_rla=0;
                    var mod_extra=0;
                    $(document).find(".lvl_sim_change_trg").each(function(){
                        if($(this).hasClass("reso"))
                        {
                            reso_total += parseInt($(this).val());
                            reso_count++;
                        }
                        else
                        {
                            switch($(this).val()) {
                                case "RLA":
                                    mod_rla++;
                                    break;
                                case "VRLA":
                                    mod_vrla++;
                                    break;
                                case "SBUL":
                                    mod_sbul++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    var base_portal_range = 160 * Math.pow((reso_total/8),4);
                    for(var i=1;i<=4;i++)
                    {
                        var mod_load=0;
                        if(mod_vrla>0)
                        {
                            mod_vrla--;
                            mod_load = 7;
                        }
                        else if(mod_sbul>0)
                        {
                            mod_sbul--;
                            mod_load=5;
                        }
                        else if(mod_rla>0)
                        {
                            mod_rla--;
                            mod_load=2;
                        }
                        if(i==1)
                            /*mod_extra += 1* mod_load;*/
                            mod_extra += mod_load;
                        else if(i==2)
                            mod_extra += 0.25* mod_load;
                        else
                            mod_extra += 0.125* mod_load;
                    }
function precise_round(num, decimals) {
var t=Math.pow(10, decimals);
 return (Math.round((num * t) + (decimals>0?1:0)*(Math.sign(num) * (10 / Math.pow(100, decimals)))) / t).toFixed(decimals);
    }
                    var new_portal_range = base_portal_range * ((mod_extra===0)?1:mod_extra);
      $("#randdetails tbody tr:first td:nth-child(4) a").text(precise_round((new_portal_range/1000),3)+'km');
                                                   window.plugin.lira_range_Simulator.remove(window.plugin.lira_range_Simulator.previousGUID);

 //alert("reso total: "+reso_total+"<br>reso_count: "+reso_count+"<br>base_portal_range: "+base_portal_range+"<br>mod_extra: "+mod_extra+"<br>portal_range: "+new_portal_range);
                window.plugin.lira_range_Simulator.draw(window.plugin.lira_range_Simulator.previousGUID,new_portal_range);
            });
    	/********/
    	/* HOOK */
    	/********/

        /* portal data loaded */
    var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
       if(!isMobile)
        window.addHook('portalSelected',window.plugin.lira_range_Simulator.removePrev);
    	window.addHook('portalDetailLoaded',window.plugin.lira_range_Simulator.onPortalDetailLoaded);
    	console.log('Lira Tool: plugin started');
    };





    /*************************/
    /* onPortalDetailsLoaded */
    /*************************/

    window.plugin.lira_range_Simulator.onPortalDetailLoaded = function(data){
              //alert(JSON.stringify(data.details.resonators)+JSON.stringify(data.details.mods));
                                           window.plugin.lira_range_Simulator.remove(window.plugin.lira_range_Simulator.previousGUID);
        window.plugin.lira_range_Simulator.previousGUID=data.guid;
            window.plugin.lira_range_Simulator.orignal_portal_data={
            	resonators:data.details.resonators,
                mods:data.details.mods
            };

       };
//-------- end of send portal details to server



    // PLUGIN END //////////////////////////////////////////////////////////

    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);

    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();

} // wrapper end

// inject code into site context
var script = document.createElement('script');
var info = {};

if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {

	version: GM_info.script.version,
	name: GM_info.script.name,
	description: GM_info.script.description

};

script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
